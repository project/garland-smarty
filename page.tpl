<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="{$language}" lang="{$language}">
  <head>
    <title>{$head_title}</title>
    {$head}
    {$styles}
    {$scripts}
    <style type="text/css" media="all">@import "{$base_path}{$path_to_theme}/fewo24.css";</style>
    <style type="text/css" media="print">@import "{$base_path}{$path_to_theme}/print.css";</style>
    <!--[if lt IE 7]>
    <style type="text/css" media="all">@import "{$base_path}{$path_to_theme}/fix-ie.css";</style>
    <![endif]-->
  </head>
  <body{body_class sidebar_left=$sidebar_left sidebar_right=$sidebar_right}>

<!-- Layout -->
  <div id="header-region" class="clear-block">{$header}</div>

    <div id="wrapper">
    <div id="container" class="clear-block">

      <div id="header">
        <div id="logo-floater">
          {if $logo || $site_title}
            <h1><a href="{$base_path|check_url}" title="{$site_title}">
            {if $logo}
              <img src="{$logo|check_url}" alt="{$site_title}" id="logo" />
            {/if}
            {$site_html}</a></h1>
          {/if}
        </div>

        {if isset($primary_links)}
          {theme_links links=$primary_links class='links primary-links'}
        {/if}
        {if isset($secondary_links)}
          {theme_links links=$secondary_links class='links secondary-links'}
        {/if}

      </div> <!-- /header -->

      {if $sidebar_left}
        <div id="sidebar-left" class="sidebar">
          {if $search_box}<div class="block block-theme">{$search_box}</div>{/if}
          {$sidebar_left}
        </div>
      {/if}

      <div id="center"><div id="squeeze"><div class="right-corner"><div class="left-corner">
          {if $breadcrumb}{$breadcrumb}{/if}
          {if $mission}<div id="mission">{$mission}</div>{/if}

          {if $tabs}<div id="tabs-wrapper" class="clear-block">{/if}
          {if $title}<h2{if $tabs} class="with-tabs"{/if}>{$title}</h2>{/if}
          {if $tabs}{$tabs}</div>{/if}

          {if isset($tabs2)}{$tabs2}{/if}

          {if $help}{$help}{/if}
          {if $messages}{$messages}{/if}
          {$content}
          <span class="clear"></span>
          {$feed_icons}
          <div id="footer">{$footer_message}</div>
      </div></div></div></div> <!-- /.left-corner, /.right-corner, /#squeeze, /#center -->

      {if $sidebar_right}
        <div id="sidebar-right" class="sidebar">
          {if !$sidebar_left && $search_box}<div class="block block-theme">{$search_box}</div>{/if}
          {$sidebar_right}
        </div>
      {/if}

    </div> <!-- /container -->
  </div>
<!-- /layout -->

  {$closure}
  </body>
</html> 
