{*{php}phptemplate_comment_wrapper(NULL, $node->type);{/php}*}

<div id="node-{$node->nid}" class="node{if $sticky} sticky{/if}{if !$status} node-unpublished{/if}">

{$picture}

{if $page == 0}
  <h2><a href="{$node_url}" title="{$title}">{$title}</a></h2>
{/if}

  {if $submitted}
    <span class="submitted">{php}print t('!date — !username', array('!username' => theme('username', $node), '!date' => format_date($node->created)));{/php}</span>
  {/if}

  <div class="content">
    {$content}
  </div>

  <div class="clear-block clear">
    <div class="meta">
    {if $taxonomy}
      <div class="terms">{$terms}</div>
    {/if}
    </div>

    {if $links}
      <div class="links">{$links}</div>
    {/if}
  </div>

</div>
