<div id="block-{$block->module}-{$block->delta}" class="clear-block block block-{$block->module}">

{if $block->subject}
  <h2>{$block->subject}</h2>
{/if}

  <div class="content">{$block->content}</div>
</div>
