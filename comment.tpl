<div class="comment{if $comment->new} comment-new{/if}{if $comment->status == COMMENT_NOT_PUBLISHED} comment-unpublished{/if} {$zebra}">

  <div class="clear-block">
  {if $submitted}
    <span class="submitted">{php}print t('!date — !username', array('!username' => theme('username', $comment), '!date' => format_date($comment->timestamp))){/php}</span>
  {/if}

  {if $comment->new}
    <a id="new"></a>
    <span class="new">{php}print drupal_ucfirst($comment->new){/php}</span>
  {/if}

  {$picture}

    <h3>{$title}</h3>

    <div class="content">
      {$content}
    </div>

  </div>

  {if $links}
    <div class="links">{$links}</div>
  {endif}
</div>
