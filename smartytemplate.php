<?php

/**
 * Allow themable wrapping of all comments.
 */
function smarty_comment_wrapper($content, $type = null) {
  static $node_type;
  if (isset($type)) $node_type = $type;

  if (!$content || $node_type == 'forum') {
    return '<div id="comments">'. $content . '</div>';
  }
  else {
    return '<div id="comments"><h2 class="comments">'. t('Comments') .'</h2>'. $content .'</div>';
  }
}

/**
 * Override or insert Smarty variables into the templates.
 */
function  _smarty_variables($hook, $variables) {
  switch($hook) {
    case 'page' :
      $variables['base_path'] = base_path();
      $variables['path_to_theme'] = path_to_theme();
      
      // Prepare header
      $site_fields = array();
      if ($variables['site_name']) {
        $site_fields[] = check_plain($variables['site_name']);
      }
      if ($variables['site_slogan']) {
        $site_fields[] = check_plain($variables['site_slogan']);
      }
      $variables['site_title'] = implode(' ', $site_fields);
      $site_fields[0] = '<span>'. $site_fields[0] .'</span>';
      $variables['site_html'] = implode(' ', $site_fields);
      
      // Hook into color.module
      if (module_exists('color')) {
        _color_page_alter($variables);
      }
      
      break;
      
    case 'node' :
      $variables['node_type_name'] = node_get_types('name', $variables['node']);
      
    default:
      $variables['base_path'] = base_path();
      $variables['path_to_theme'] = path_to_theme();
      break;
  }
  return $variables;
}

function smarty_field(&$node, &$field, &$items, $teaser, $page) {
  $field_empty = TRUE;
  foreach ($items as $delta => $item) {
    if (!empty($item['view']) || $item['view'] === "0") {
      $field_empty = FALSE;
      break;
    }
  }

  $variables = array(
    'node' => $node,
    'field' => $field,
    'field_type' => $field['type'],
    'field_name' => $field['field_name'],
    'field_type_css' => strtr($field['type'], '_', '-'),
    'field_name_css' => strtr($field['field_name'], '_', '-'),
    'label' => t($field['widget']['label']),
    'label_display' => isset($field['display_settings']['label']['format']) ? $field['display_settings']['label']['format'] : 'above',
    'field_empty' => $field_empty,
    'items' => $items,
    'teaser' => $teaser,
    'page' => $page,
  );

  return _smarty_callback('field', $variables, array('field-'. $field['field_name']));
}

?>
